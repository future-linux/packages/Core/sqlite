# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=(sqlite sqlite-tcl sqlite-analyzer lemon)
pkgbase=sqlite
pkgver=3.39.2
pkgrel=2
pkgdesc="A C library that implements an SQL database engine"
arch=('x86_64')
url="https://www.sqlite.org/"
license=('custom:Public Domain')
makedepends=('tcl' 'readline' 'zlib')
options=('!emptydirs')
source=(https://www.sqlite.org/2022/${pkgbase}-src-$(printf "%i%.2i%.2i%.2i" ${pkgver//./ }).zip
    sqlite-lemon-system-template.patch)
sha256sums=(e933d77000f45f3fbc8605f0050586a3013505a8de9b44032bd00ed72f1586f0
    55746d93b0df4b349c4aa4f09535746dac3530f9fd6de241c9f38e2c92e8ee97)

prepare() {
    cd ${pkgbase}-src-$(printf "%i%.2i%.2i%.2i" ${pkgver//./ })

    # patch taken from Fedora
    # https://src.fedoraproject.org/rpms/sqlite/blob/master/f/sqlite.spec
    patch -Np1 -i ${srcdir}/sqlite-lemon-system-template.patch
}

build() {
    # sqlite
    cd ${pkgbase}-src-$(printf "%i%.2i%.2i%.2i" ${pkgver//./ })

    export CPPFLAGS="${CPPFLAGS}             \
        -DSQLITE_ENABLE_COLUMN_METADATA=1    \
        -DSQLITE_ENABLE_UNLOCK_NOTIFY        \
        -DSQLITE_ENABLE_DBSTAT_VTAB=1        \
        -DSQLITE_ENABLE_FTS3_TOKENIZER=1     \
        -DSQLITE_SECURE_DELETE               \
        -DSQLITE_ENABLE_STMTVTAB             \
        -DSQLITE_MAX_VARIABLE_NUMBER=250000  \
        -DSQLITE_MAX_EXPR_DEPTH=10000        \
        -DSQLITE_ENABLE_MATH_FUNCTIONS"

    ${configure}          \
        --disable-static  \
        --enable-fts3     \
        --enable-fts4     \
        --enable-fts5     \
        --enable-rtree    \
        TCLLIBDIR=/usr/lib64/${pkgbase}${pkgver}

    sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
    make

    # build additional tools
    make showdb showjournal showstat4 showwal sqldiff sqlite3_analyzer
}

package_sqlite() {
    pkgdesc="A C library that implements an SQL database engine"
    depends=('readline' 'zlib')

    cd ${pkgbase}-src-$(printf "%i%.2i%.2i%.2i" ${pkgver//./ })

    make DESTDIR=${pkgdir} install

    install -m755 showdb showjournal showstat4 showwal sqldiff ${pkgdir}/usr/bin/

    # install manpage
    install -m755 -d ${pkgdir}/usr/share/man/man1
    install -m644 sqlite3.1 ${pkgdir}/usr/share/man/man1/

    # split out tcl extension
    mkdir ${srcdir}/tcl
    mv ${pkgdir}/usr/lib64/sqlite* ${srcdir}/tcl
}

package_sqlite-tcl() {
    pkgdesc="sqlite Tcl Extension Architecture (TEA)"
    depends=('sqlite')

    cd ${pkgbase}-src-$(printf "%i%.2i%.2i%.2i" ${pkgver//./ })

    install -m755 -d ${pkgdir}/usr/lib64
    mv ${srcdir}/tcl/* ${pkgdir}/usr/lib64

    # install manpage
    install -m755 -d ${pkgdir}/usr/share/man/mann
    install -m644 autoconf/tea/doc/sqlite3.n ${pkgdir}/usr/share/man/mann/
}

package_sqlite-analyzer() {
    pkgdesc="An analysis program for sqlite3 database files"
    depends=('sqlite' 'tcl')

    cd ${pkgbase}-src-$(printf "%i%.2i%.2i%.2i" ${pkgver//./ })

    install -m755 -d ${pkgdir}/usr/bin
    install -m755 sqlite3_analyzer ${pkgdir}/usr/bin
}

package_lemon() {
    # https://www.sqlite.org/lemon.html
    pkgdesc="A parser generator"
    depends=('glibc')

    cd ${pkgbase}-src-$(printf "%i%.2i%.2i%.2i" ${pkgver//./ })

    # ELF file ('usr/bin/lemon') lacks FULL RELRO, check LDFLAGS. - no fix found so far
    install -Dm755 lemon ${pkgdir}/usr/bin/lemon
    install -Dm644 lempar.c ${pkgdir}/usr/share/lemon/lempar.c
}
